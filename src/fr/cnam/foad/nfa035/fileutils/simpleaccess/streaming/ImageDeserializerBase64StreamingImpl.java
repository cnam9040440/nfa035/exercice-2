package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * classe permettant la déserialisation à partir d'un encodage en base64
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<ImageByteArrayFrame > implements ImageStreamingDeserializer<ImageByteArrayFrame > {

    OutputStream outputStream;
    public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
        this.outputStream = deserializationOutput;
    }

    @Override
    /**
     *
     * methode pour récupérer un outputStream
     */
    public OutputStream getSourceOutputStream() {
        return outputStream;
    }


    @Override
    /**
     *
     * methode pour récupérer un inputStream à partir d'un ImageByteArrayFrame
     *
     * @param media
     */
    public InputStream getDeserializingStream(ImageByteArrayFrame media) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(media.outputStream.toByteArray());
        return new Base64InputStream(byteArrayInputStream);
    }
}
