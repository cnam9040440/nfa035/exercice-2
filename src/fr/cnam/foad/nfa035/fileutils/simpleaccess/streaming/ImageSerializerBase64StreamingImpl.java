package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;
import java.nio.file.Files;

import org.apache.commons.codec.binary.Base64OutputStream;

/**
 * classe permettant la serialisation avec un encodage en base64
 */
public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer<File,  ImageByteArrayFrame > implements ImageStreamingSerializer<File,  ImageByteArrayFrame >  {


	@Override
	/**
	 *
	 * Methode pour récupérer un outputStream
	 *
	 * @param media
	 */
	public OutputStream getSerializingStream(ImageByteArrayFrame media) throws IOException {
		return  new Base64OutputStream(media.outputStream);
	}

	@Override
	/**
	 *
	 * Methode pour récupérer un inputStream à partir d'un File
	 *
	 * @param source
	 */
	public InputStream getSourceInputStream(File source) throws IOException {
		InputStream fileInput = new FileInputStream(source);
		return fileInput;
	}
}

