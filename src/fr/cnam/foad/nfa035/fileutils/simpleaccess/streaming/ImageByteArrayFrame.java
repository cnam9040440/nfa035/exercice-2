package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * classe permettant de créer l'object ImageByteArrayFrame
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia<Object>{

	ByteArrayOutputStream outputStream;


	public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
        this.outputStream = byteArrayOutputStream;
    }


    @Override
    /**
     * methode permettant de récupérer l'ouput de l'image encodé
     */
    public OutputStream getEncodedImageOutput() throws IOException {
        return outputStream;
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return null;
    }
}
